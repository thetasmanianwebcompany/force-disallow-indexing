<?php
/*
Plugin Name:  Force Disallow Indexing
Description:  Disallow indexing of your site. Dynamically add this to your staging and development sites via composer for a robust set and forget system.
Version:      1.0.0
Author:       The Tasmanian Web Company
Author URI:   https://thetasmanianwebcompany.com.au/
License:      MIT License
*/

if (! is_admin()) {
	add_action('pre_option_blog_public', '__return_zero');
}
